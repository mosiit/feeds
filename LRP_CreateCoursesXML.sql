USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_CreateCoursesXML]    Script Date: 1/20/2017 10:24:06 AM ******/
DROP PROCEDURE [dbo].[LRP_CreateCoursesXML]
GO

/****** Object:  StoredProcedure [dbo].[LRP_CreateCoursesXML]    Script Date: 1/20/2017 10:24:06 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[LRP_CreateCoursesXML]
AS
SET NOCOUNT ON 
-- ==============================================================================================================
-- Based on code by D. Jacob (LRP_CreateProductsXML)
-- Created by: H. Sheridan, 1/20/2017
-- Purpose: Returns a list of courses plus available capacity ordered by date and time in XML format.
-- Used by: Web Site
--
-- Notes:   The structure of this code varies widely from the products feed.  The products feed is limited to
--			30 days out because it references the LV_PERFORMANCE_CAPACITY_AVAILABLITY view.  Courses go on sale 
--			in January but are taken in the summer months.  For that reason, the code had to change.  
--
--			I used the 'show execution plan' tool heavily here to figure out how I needed to parse out the data 
--			into workable chunks in order to get good performance.  When written in the same style as the products 
--			feed, this code took 30 seconds to run.  That was in test, under virtually no stress - unacceptable.
--
--			Although it might looks like a lot of parsing out of data (and it is), using this style got the 
--			performance to 5 seconds.  

-- ==============================================================================================================

DECLARE @OutputXML XML,
		@GradeID INT

SELECT  @GradeID = [id]
FROM    TR_INV_CONTENT (NOLOCK)
WHERE   [description] = 'Grade Level'

--DECLARE @CourseGroups TABLE (Course VARCHAR(100))

--INSERT  INTO @CourseGroups VALUES  ('Summer Courses')

--
-- Populating @courses table with all data needed for courses.xml
--
DECLARE @courses TABLE (EventStartDate CHAR(10),
                        EventEndDate CHAR(10),
                        EventStartTime VARCHAR(8),
                        EventEndTime VARCHAR(8),
                        GradeStart VARCHAR(2),
                        GradeEnd VARCHAR(2),
                        InStock INT,
                        EventNumber INT,
                        EventTitleLong VARCHAR(100),
                        UNIQUE CLUSTERED (EventStartDate, EventTitleLong, EventNumber))

DECLARE	@perfs TABLE	(
						performance_no			INT,
						performance_zone		INT,
						performance_zone_map	INT,
						performance_start_dt	VARCHAR(10),	
						performance_end_dt		VARCHAR(10),	
						performance_time		VARCHAR(30),
						performance_end_time	VARCHAR(5),
			            production_no			INT,
			            production_name_long	VARCHAR(MAX)
						UNIQUE CLUSTERED (performance_no, performance_zone, performance_zone_map)
						)


DECLARE	@seats TABLE	(
						perf_no		INT,
						zone_no		INT,
						zmap_no		INT,
						capacity	INT,
						available	INT
						UNIQUE CLUSTERED (perf_no, zone_no, zmap_no)
						)

INSERT INTO @perfs
     SELECT DISTINCT
			perf.performance_no,
			perf.performance_zone,
			perf.performance_zone_map,
            CONVERT(VARCHAR(10), perf.performance_dt, 111) AS performance_start_dt,
            CONVERT(VARCHAR(10), DATEADD(dd, 4, perf.performance_dt), 111) AS performance_end_dt,
            perf.performance_time,
            perf.performance_end_time,
            perf.production_no,
            perf.production_name_long
     FROM   [dbo].[LV_PRODUCTION_ELEMENTS_PERFORMANCE] (NOLOCK) perf
     WHERE  perf.performance_name_long <> ''
            AND perf.title_name = 'Summer Courses'  --IN (SELECT Course FROM @CourseGroups)
            AND (perf.performance_type_name <> 'Buyout'
            AND perf.production_name NOT LIKE '%Buyout%'
            )
            AND perf.is_generic_title = 'N'
            AND perf.performance_avail_sale_ind = 'Y'
		
INSERT  INTO @seats
        SELECT  seat.perf_no,
                seat.zone_no,
                seat.zmap_no,
                SUM(CASE seat.status_code
                      WHEN 'BLK' THEN 0
                      WHEN 'NS' THEN 0
                      ELSE 1
                    END) AS capacity,
                cap.available
        FROM    [dbo].[TX_PERF_SEAT] (NOLOCK) seat
        INNER JOIN @perfs perf ON perf.performance_no = seat.perf_no
                                  AND perf.performance_zone = seat.zone_no
                                  AND perf.performance_zone_map = seat.zmap_no
        INNER JOIN dbo.T_PERF_ZONE_SUMMARY (NOLOCK) cap ON perf.performance_no = cap.perf_no
                                                           AND perf.performance_zone = cap.zone_no
        WHERE   seat.[seat_no] >= 0
                AND seat.[pkg_no] >= 0
        GROUP BY seat.perf_no,
                seat.zone_no,
                seat.zmap_no,
                cap.available

--select 'debug', * from @courses

INSERT INTO @courses
     SELECT DISTINCT
            performance_start_dt,
            performance_end_dt,
            perf.performance_time,
            perf.performance_end_time,
            SUBSTRING(grd.value, 1, PATINDEX('%-%', grd.value) - 1) AS 'GradeStart',
            SUBSTRING(grd.value, PATINDEX('%-%', grd.value) + 1, LEN(grd.value)) AS 'GradeEnd',
            ISNULL(seat.available, 0) AS InStock,
            perf.production_no,
            perf.production_name_long
	 FROM	@perfs perf
	 LEFT JOIN @seats seat
	 ON perf.performance_no = seat.perf_no
            AND perf.performance_zone = seat.zone_no
            AND perf.performance_zone_map = seat.zmap_no
     LEFT JOIN [dbo].[LV_PRODUCTION_ELEMENTS_PRODUCTION] prod ON prod.[production_no] = perf.[production_no]
     --INNER JOIN dbo.T_PERF_ZONE_SUMMARY (NOLOCK) cap ON perf.performance_no = cap.perf_no
     --                                                   AND perf.performance_zone = cap.zone_no
     LEFT JOIN [dbo].[T_INVENTORY] AS inv ON prod.[production_no] = inv.[inv_no]
     LEFT OUTER JOIN [dbo].[TX_INV_CONTENT] AS grd ON grd.[inv_no] = inv.[inv_no]
	                                                       AND grd.[content_type] = @GradeID

UPDATE  @courses
SET     EventTitleLong = REPLACE(REPLACE(REPLACE(EventTitleLong, '&amp;', '&'), NCHAR(8211), '-'), NCHAR(8212), '-')
-- NCHAR(8211) is en dash
-- NCHAR(8212) is em dash

--select 'debug', * from @courses

--
-- Populting venues table with all distinct venue + memberonly + softcapacity options
-- 

DECLARE @venues TABLE	(
	VenueId				INT IDENTITY(1,1),
	Venue				VARCHAR(100)
)

INSERT INTO @venues
(
	Venue
)
SELECT DISTINCT 'Courses'

--SELECT 'venues', * FROM @venues

--
-- Getting all shows available in each venue
-- 

DECLARE @shows TABLE	
(
	showID				INT IDENTITY, 
	venueID				INT,
	EventTitleLong		VARCHAR(500),
	EventNumber			INT,
	GradeStart			VARCHAR(2),
	GradeEnd			VARCHAR(2)
)

INSERT INTO @shows
(
	VenueID,
	EventTitleLong,
	EventNumber,
	GradeStart,
	GradeEnd
)
SELECT	DISTINCT 
	1,
	p.EventTitleLong,
	p.EventNumber,
	p.GradeStart,
	p.GradeEnd
FROM    @courses p

--SELECT 'shows', * FROM @shows

--
-- Getting all unique show dates for each venue/Show combination
--
DECLARE @showDates TABLE	
(
	VenueId	INT,
	ShowId INT,
	EventStartDate VARCHAR(10),
	EventEndDate VARCHAR(10),
	EventStartTime VARCHAR(8),
    EventEndTime VARCHAR(8),
	InStock INT
)

INSERT INTO @showDates
(
	VenueId,
	ShowId,
	EventStartDate,
	EventEndDate,
	EventStartTime,
	EventEndTime,
	InStock
)
SELECT	DISTINCT 
	1,
	s.ShowID,
	p.EventStartDate,
	p.EventEndDate,
	p.EventStartTime,
	p.EventEndTime,
	p.InStock
FROM    @courses p
INNER JOIN @shows s
	ON s.EventTitleLong = p.EventTitleLong
	AND s.EventNumber = p.EventNumber

--SELECT 'showDates', * FROM @showDates

SET @OutputXML = 
(SELECT v.venue AS 'title',
	(
	SELECT	s.EventTitleLong AS 'title',
			s.EventNumber AS 'eventnumber',
			s.GradeStart AS 'startgrade',
			s.GradeEnd AS 'endgrade',
			(
			SELECT sdt.EventStartDate AS 'startdate',
				   sdt.EventEndDate AS 'enddate',
				   sdt.EventStartTime AS 'starttime',
				   sdt.EventEndTime AS 'endtime',
				   sdt.InStock AS 'instock'
			FROM	@showDates sdt
			WHERE	sdt.ShowId = s.ShowId
			AND		sdt.VenueId = s.VenueId
			ORDER BY sdt.EventStartDate
			FOR XML PATH ('date'), TYPE
			)			
	FROM	@shows s
	WHERE	s.VenueId = v.VenueId
	FOR XML PATH ('show'), ROOT ('shows'), TYPE	
	)
FROM	@venues v
ORDER BY v.Venue
FOR XML PATH ('venue'), ROOT ('products'), TYPE
)

SELECT @OutputXML AS outputXML

GO
