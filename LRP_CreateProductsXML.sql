USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_CreateProductsXML]    Script Date: 12/13/2018 1:43:41 PM ******/
DROP PROCEDURE [dbo].[LRP_CreateProductsXML]
GO

/****** Object:  StoredProcedure [dbo].[LRP_CreateProductsXML]    Script Date: 12/13/2018 1:43:41 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[LRP_CreateProductsXML]
(
	@NumOfDays INT = 0 -- 0 for today's schedule
)

AS

SET NOCOUNT ON 
/*
Created by: D. Jacob 05/02/2016
Purpose: Returns a list of activities plus available capacity ordered by date and time in XML format.
Used by Digital Signage
Pass in 0 for today's schedule 

Modified by:  H. Sheridan, 12/13/2018
Modification: Remove the venue capacity to see if it fixes issues with the today feed.
*/

DECLARE @OutputXML XML 

DECLARE @VenuesForDigitalSignage TABLE (Venue varchar(100))
--CREATE TABLE #VenuesForDigitalSignage (Venue varchar(100) NOT NULL DEFAULT (''))


INSERT INTO @VenuesForDigitalSignage ([Venue])
VALUES
('4-D Theater'),
('Butterfly Garden'),
('Hayden Planetarium'),
('Drop-In Activities'),
('Live Presentations'),
('Mugar Omni Theater'),
('School Lunch')
--('Thrill Ride 360')

--
-- Populating @prods table with all data needed for products.xml
--
DECLARE @prods TABLE (
	EventDate CHAR(10),
	EventTime VARCHAR(8),
	EventEnd VARCHAR(8),
	Venue VARCHAR(100),
	Location VARCHAR(100),
	SoftCapacity INT,
	Instock INT,
	EventNumber INT,
	MemberOnly INT,
	SchoolOnly INT,
	EventTitleLong VARCHAR(100),
	ItemLength INT
	UNIQUE CLUSTERED (EventDate, EventTime, EventTitleLong, EventNumber, Instock)
)

INSERT INTO @prods
	(EventDate,
	EventTime,
	EventEnd,
	Venue,
	Location,
	SoftCapacity,
	Instock,
	EventNumber,
	MemberOnly,
	SchoolOnly,
	EventTitleLong,
	ItemLength)
SELECT DISTINCT
CONVERT(VARCHAR(10),perf.performance_dt,111) AS EventDate,
	perf.performance_time AS EventTime,
	perf.performance_end_time AS EventEnd,
	CASE perf.title_name 
		WHEN 'Hayden Planetarium' THEN 'Charles Hayden Planetarium'
		ELSE perf.title_name 
	END AS Venue,
	CASE perf.title_name
		WHEN 'Drop-In Activities' THEN perf.performance_location 
		WHEN 'Live Presentations' THEN perf.performance_location 
		WHEN 'Hayden Planetarium' THEN 'Planetarium'
		ELSE perf.title_name
	END	AS location,
	CASE 
		WHEN perf.title_name = 'Drop-In Activities' THEN '30000'
		WHEN perf.title_name = 'Live Presentations' THEN '30000'
	    ELSE dbo.[LF_GetCapacity](perf.[performance_no],perf.[performance_zone],'Capacity')
        --ELSE ISNULL(cap.capacity, 0) 
	END AS SoftCapacity, 
	CASE 
		WHEN perf.title_name = 'Drop-In Activities' THEN '30000'
		WHEN perf.title_name = 'Live Presentations' THEN '30000'
		ELSE dbo.[LF_GetCapacity](perf.[performance_no],perf.[performance_zone],'Available')
        --ELSE ISNULL(cap.available, 0) 
	END AS Instock, 
	perf.production_no AS EventNumber,
	CASE perf.performance_type_name
		WHEN 'Member Event' THEN 1
		ELSE 0
	END AS MemberOnly,
	CASE perf.performance_type_name 
		WHEN 'School Only' THEN 1
		ELSE 0
	END AS SchoolOnly,
	perf.production_name_long AS EventTitleLong,
	DATEDIFF(mi, CAST(perf.performance_time AS DATETIME), CAST(perf.performance_end_time AS DATETIME)) AS ItemLength 
FROM dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE perf
    --INNER JOIN @VenuesForDigitalSignage AS ven ON ven.[Venue] = perf.[title_name]
  --  LEFT JOIN [dbo].[LV_PERFORMANCE_CAPACITY_AVAILABLITY] cap
		--ON cap.production_no = perf.production_no
		--AND cap.performance_date = perf.performance_date
		--AND cap.performance_time = perf.performance_time
WHERE DATEDIFF(d, GETDATE(), perf.performance_dt) >= 0 AND DATEDIFF(d, GETDATE(), perf.performance_dt) <= @NumOfDays
	AND perf.performance_name_long <> ''
	AND perf.title_name IN (SELECT Venue FROM @VenuesForDigitalSignage)
	AND (perf.performance_type_name <> 'Buyout' AND perf.production_name NOT LIKE '%Buyout%')
	AND perf.is_generic_title = 'N' 

--select 'debug', * from @prods

UPDATE @prods
SET EventTitleLong = REPLACE(REPLACE(REPLACE(EventTitleLong, '&amp;', '&'), NCHAR(8211), '-'),NCHAR(8212), '-')
-- NCHAR(8211) is en dash
-- NCHAR(8212) is em dash

--SELECT 'prods', * FROM @prods
--
-- Populting venues table with all distinct venue + memberonly + softcapacity options
-- 

DECLARE @venues TABLE	(
	VenueId				INT IDENTITY(1,1),
	Venue				VARCHAR(100),
	MemberOnly			INT
	--SoftCapacity		INT
)

INSERT INTO @venues
(
	Venue,
	MemberOnly
	--SoftCapacity
)
SELECT DISTINCT 
	Venue,	
	MemberOnly
	--SoftCapacity
FROM @prods 

--SELECT 'venues', * FROM @venues

--
-- Getting all shows available in each venue
-- 

DECLARE @shows TABLE	
(
	showID	INT IDENTITY, 
	venueID INT,
	EventTitleLong		VARCHAR(500),
	Location			VARCHAR(100),
	EventNumber			INT,
	ItemLength			INT
)

INSERT INTO @shows
(
	v.VenueID,
	EventTitleLong,
	Location,
	EventNumber,
	ItemLength
)
SELECT	DISTINCT 
	v.Venueid,
	p.EventTitleLong,
	p.Location,
	p.EventNumber,
	p.ItemLength
FROM    @prods p
INNER JOIN	@venues v
	ON	p.Venue = v.Venue
	AND	 p.MemberOnly = v.MemberOnly
	--AND p.SoftCapacity = v.SoftCapacity

--SELECT 'shows', * FROM @shows

--
-- Getting all unique show dates for each venue/Show combination
--
DECLARE @showDates TABLE	
(
	VenueId	INT,
	ShowId INT,
	EventDate VARCHAR(10)
)

INSERT INTO @showDates
(
	VenueId,
	ShowId,
	EventDate
)
SELECT	DISTINCT 
	v.VenueId,
	s.ShowID,
	p.EventDate
FROM    @prods p
INNER JOIN	@venues v
	ON	p.Venue = v.Venue
	AND	 p.MemberOnly = v.MemberOnly
	--AND p.SoftCapacity = v.SoftCapacity
INNER JOIN @shows s
	ON s.EventTitleLong = p.EventTitleLong
	AND s.Location = p.Location
	AND s.EventNumber = p.EventNumber
	AND s.ItemLength = p.ItemLength

--SELECT 'showDates', * FROM @showDates

--
-- Getting all unique show times for each venue/Show/Date combination
--
DECLARE @showTimes TABLE	
(
	VenueId	INT,
	ShowId INT,
	EventDate VARCHAR(10),
	EventTime VARCHAR(8),
	EventEnd VARCHAR(8),
	SchoolOnly INT,
	InStock INT
)

INSERT INTO @showTimes
(
	VenueId,
	ShowId,
	EventDate,
	EventTime,
	EventEnd,
	SchoolOnly,
	InStock
)
SELECT	DISTINCT 
	v.VenueId,
	s.ShowID,
	p.EventDate,
	p.EventTime,
	p.EventEnd,
	p.SchoolOnly,
	p.InStock
FROM    @prods p
INNER JOIN	@venues v
	ON	p.Venue = v.Venue
	AND	 p.MemberOnly = v.MemberOnly
	--AND p.SoftCapacity = v.SoftCapacity
INNER JOIN @shows s
	ON s.EventTitleLong = p.EventTitleLong
	AND s.Location = p.Location
	AND s.EventNumber = p.EventNumber
	AND s.ItemLength = p.ItemLength

--SELECT 'showTimes', * FROM @showTimes


SET @OutputXML = 
(SELECT v.venue AS 'title',
	v.MemberOnly AS 'memberonly',
	--v.SoftCapacity as 'capacity',
	(
	SELECT	s.EventTitleLong AS 'title',
			s.Location AS 'location',
			s.ItemLength as 'length',
			s.EventNumber AS 'eventnumber',
			(
			SELECT sdt.EventDate AS 'eventdate',
					(
					SELECT	times.SchoolOnly AS 'starttime/@schoolonly',
							times.EventTime AS 'starttime',
							times.SchoolOnly AS 'endtime/@schoolonly',
							times.EventEnd AS 'endtime',
							times.InStock AS 'instock'
					FROM	@showTimes times
					WHERE	times.ShowId = sdt.ShowId
						AND		times.VenueId = sdt.VenueId
						AND		times.EventDate = sdt.EventDate
					ORDER BY times.EventDate, times.EventTime, times.EventEnd
					FOR XML PATH ('time'), ROOT ('times'), TYPE
					)
			FROM	@showDates sdt
			WHERE	sdt.ShowId = s.ShowId
			AND		sdt.VenueId = s.VenueId
			ORDER BY sdt.EventDate
			FOR XML PATH ('date'), TYPE
			)			
	FROM	@shows s
	WHERE	s.VenueId = v.VenueId
	FOR XML PATH ('show'), ROOT ('shows'), TYPE	
	)
FROM	@venues v
ORDER BY v.Venue
FOR XML PATH ('venue'), ROOT ('products'), TYPE
)

SELECT @OutputXML AS outputXML


DONE:



GO


