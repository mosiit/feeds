USE [impresario]
GO

/****** Object:  StoredProcedure [dbo].[LRP_CreateProductsXML_GT30Days_FixedSeats]    Script Date: 11/20/2018 12:58:19 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[LRP_CreateProductsXML_GT30Days]
(
	@NumOfDays INT = 0 -- 0 for today's schedule
)

AS
SET NOCOUNT ON 
-- ================================================================================================================
-- Based on code by D. Jacob (LRP_CreateProductsXML)
-- Created by: H. Sheridan, 1/24/2017
-- Purpose: Returns a list of products plus available capacity ordered by date and time in XML format.
-- Used by: Digital Signage
--
-- Notes:   This is a new version of the products feed that doesn't depend on LV_PERFORMANCE_CAPACITY_AVAILABLITY.
--			That view is held to 30 days, which has limited the usage of this feed in the past. I took the code
--			from that view and used it here (without the number of days limit) to go into the future.  I parse
--			out chunks of data in order to get better performance.  That was an issue in that past when we tried
--			using LV_PERFORMANCE_CAPACITY_AVAILABLITY in an expanded days kind of way.  
--			
--			I timed this code using various values for the @NumOfDays parameter.  6 months took under 1 minute,
--			as did 9 months.  Whenever this moves into production, we should watch the timings but I don't expect 
--			poor performance.
--			
-- ================================================================================================================


/*
Created by: D. Jacob 05/02/2016
Purpose: Returns a list of activities plus available capacity ordered by date and time in XML format.
Used by Digital Signage
Pass in 0 for today's schedule 
*/

DECLARE @OutputXML XML 

DECLARE @VenuesForDigitalSignage TABLE 
(
	Venue varchar(100)
)

INSERT INTO @VenuesForDigitalSignage
VALUES
('4-D Theater'),
('Butterfly Garden'),
('Hayden Planetarium'),
('Drop-In Activities'),
('Live Presentations'),
('Mugar Omni Theater'),
('School Lunch')
--('Thrill Ride 360')

DECLARE	@perfs TABLE	(
						performance_no			INT,
						performance_zone		INT,
						performance_zone_map	INT,
						performance_dt			VARCHAR(10),	
						performance_time		VARCHAR(30),
						performance_end_time	VARCHAR(5),
			            production_no			INT,
			            production_name_long	VARCHAR(MAX),
						title_name				VARCHAR(30),
						performance_location	VARCHAR(MAX),
						performance_type_name	VARCHAR(30),
						venue					VARCHAR(60),
						location				VARCHAR(MAX),
						member_only				BIT,
						school_only				BIT
						UNIQUE CLUSTERED (performance_no, performance_zone, performance_zone_map)
						)

DECLARE	@seats TABLE	(
						perf_no		INT,
						zone_no		INT,
						zmap_no		INT,
						capacity	INT,
						available	INT
						UNIQUE CLUSTERED (perf_no, zone_no, zmap_no)
						)

DECLARE @prods TABLE (EventDate CHAR(10),
                      EventTime VARCHAR(8),
                      EventEnd VARCHAR(8),
                      Venue VARCHAR(100),
                      Location VARCHAR(100),
                      SoftCapacity INT,
                      Instock INT,
                      EventNumber INT,
                      MemberOnly INT,
                      SchoolOnly INT,
                      EventTitleLong VARCHAR(100),
                      ItemLength INT UNIQUE CLUSTERED (EventDate, EventTime, EventTitleLong, EventNumber))

INSERT INTO @perfs
	SELECT DISTINCT	performance_no,
					performance_zone,
					performance_zone_map,
					CONVERT(VARCHAR(10),performance_dt,111),
					performance_time,
					performance_end_time,
					production_no,
					production_name_long,
					title_name,
					performance_location,
					performance_type_name,
					CASE perf.title_name
						WHEN 'Hayden Planetarium' THEN 'Charles Hayden Planetarium'
						ELSE perf.title_name
					END AS venue,
					CASE perf.title_name
						WHEN 'Drop-In Activities' THEN perf.performance_location
						WHEN 'Live Presentations' THEN perf.performance_location
						WHEN 'Hayden Planetarium' THEN 'Planetarium'
						ELSE perf.title_name
					END AS location,
					CASE perf.performance_type_name
						WHEN 'Member Event' THEN 1
						ELSE 0
					END AS MemberOnly,
					CASE perf.performance_type_name
						WHEN 'School Only' THEN 1
						ELSE 0
					END AS SchoolOnly
	FROM	dbo.LV_PRODUCTION_ELEMENTS_PERFORMANCE (NOLOCK) perf
	WHERE DATEDIFF(d, GETDATE(), performance_dt) >= 0 AND DATEDIFF(d, GETDATE(), performance_dt) <= @NumOfDays
		AND perf.performance_name_long <> ''
		AND perf.title_name IN (SELECT Venue FROM @VenuesForDigitalSignage)
		AND (perf.performance_type_name <> 'Buyout' AND perf.production_name NOT LIKE '%Buyout%')
		AND perf.is_generic_title = 'N' 

INSERT  INTO @seats
        SELECT  seat.perf_no,
                seat.zone_no,
                seat.zmap_no,
				-- 2018-11-20, H. Sheridan - per Tessitura, use seat_status instead of status_code (deprecated)
				-- 2018-11-20, H. Sheridan - force hard coded number
				999 AS capacity,
				--SUM(CASE seat.seat_status
    --                  WHEN 6 THEN 0 -- 'BLK'
    --                  WHEN 9 THEN 0 -- 'NS'
    --                  ELSE 1
    --                END) AS capacity,
                --SUM(CASE seat.status_code
                --      WHEN 'BLK' THEN 0
                --      WHEN 'NS' THEN 0
                --      ELSE 1
                --    END) AS capacity,
                cap.available
        FROM    [dbo].[TX_PERF_SEAT] (NOLOCK) seat
        INNER JOIN @perfs perf ON perf.performance_no = seat.perf_no
                                  AND perf.performance_zone = seat.zone_no
                                  AND perf.performance_zone_map = seat.zmap_no
        INNER JOIN dbo.T_PERF_ZONE_SUMMARY (NOLOCK) cap ON perf.performance_no = cap.perf_no
                                                           AND perf.performance_zone = cap.zone_no
        WHERE   seat.[seat_no] >= 0
                AND seat.[pkg_no] >= 0
        GROUP BY seat.perf_no,
                seat.zone_no,
                seat.zmap_no,
                cap.available

-- =============================================================================================

--
-- Populating @prods table with all data needed for products.xml
--

INSERT  INTO @prods
        (EventDate,
         EventTime,
         EventEnd,
         Venue,
         Location,
         SoftCapacity,
         Instock,
         EventNumber,
         MemberOnly,
         SchoolOnly,
         EventTitleLong,
         ItemLength
        )
        SELECT DISTINCT
                CONVERT(VARCHAR(10), perf.performance_dt, 111) AS EventDate,
                perf.performance_time AS EventTime,
                perf.performance_end_time AS EventEnd,
				perf.venue,
				perf.location,
                CASE WHEN perf.title_name = 'Drop-In Activities' THEN '30000'
                     WHEN perf.title_name = 'Live Presentations' THEN '30000'
                     ELSE ISNULL(seat.capacity, 0)
                END AS SoftCapacity,
                CASE WHEN perf.title_name = 'Drop-In Activities' THEN '30000'
                     WHEN perf.title_name = 'Live Presentations' THEN '30000'
                     ELSE ISNULL(seat.available, 0)
                END AS Instock,
                perf.production_no AS EventNumber,
				perf.member_only,
				perf.school_only,
                perf.production_name_long AS EventTitleLong,
                DATEDIFF(mi, CAST(perf.performance_time AS DATETIME), CAST(perf.performance_end_time AS DATETIME)) AS ItemLength
	 FROM	@perfs perf
	 LEFT JOIN @seats seat
	 ON perf.performance_no = seat.perf_no
            AND perf.performance_zone = seat.zone_no
            AND perf.performance_zone_map = seat.zmap_no

--select 'debug', * from @perfs
--select 'debug', * from @seats
--select 'debug', * from @prods

UPDATE @prods
SET EventTitleLong = REPLACE(REPLACE(REPLACE(EventTitleLong, '&amp;', '&'), NCHAR(8211), '-'),NCHAR(8212), '-')
-- NCHAR(8211) is en dash
-- NCHAR(8212) is em dash

--SELECT 'prods', * FROM @prods
--
-- Populting venues table with all distinct venue + memberonly + softcapacity options
-- 

DECLARE @venues TABLE	(
	VenueId				INT IDENTITY(1,1),
	Venue				VARCHAR(100),
	MemberOnly			INT,
	SoftCapacity		INT
)

INSERT INTO @venues
(
	Venue,
	MemberOnly,
	SoftCapacity
)
SELECT DISTINCT 
	Venue,	
	MemberOnly,
	SoftCapacity
FROM @prods 

--SELECT 'venues', * FROM @venues

--
-- Getting all shows available in each venue
-- 

DECLARE @shows TABLE	
(
	showID	INT IDENTITY, 
	venueID INT,
	EventTitleLong		VARCHAR(500),
	Location			VARCHAR(100),
	EventNumber			INT,
	ItemLength			INT
)

INSERT INTO @shows
(
	v.VenueID,
	EventTitleLong,
	Location,
	EventNumber,
	ItemLength
)
SELECT	DISTINCT 
	v.Venueid,
	p.EventTitleLong,
	p.Location,
	p.EventNumber,
	p.ItemLength
FROM    @prods p
INNER JOIN	@venues v
	ON	p.Venue = v.Venue
	AND	 p.MemberOnly = v.MemberOnly
	AND p.SoftCapacity = v.SoftCapacity

--SELECT 'shows', * FROM @shows

--
-- Getting all unique show dates for each venue/Show combination
--
DECLARE @showDates TABLE	
(
	VenueId	INT,
	ShowId INT,
	EventDate VARCHAR(10)
)

INSERT INTO @showDates
(
	VenueId,
	ShowId,
	EventDate
)
SELECT	DISTINCT 
	v.VenueId,
	s.ShowID,
	p.EventDate
FROM    @prods p
INNER JOIN	@venues v
	ON	p.Venue = v.Venue
	AND	 p.MemberOnly = v.MemberOnly
	AND p.SoftCapacity = v.SoftCapacity
INNER JOIN @shows s
	ON s.EventTitleLong = p.EventTitleLong
	AND s.Location = p.Location
	AND s.EventNumber = p.EventNumber
	AND s.ItemLength = p.ItemLength

--SELECT 'showDates', * FROM @showDates

--
-- Getting all unique show times for each venue/Show/Date combination
--
DECLARE @showTimes TABLE	
(
	VenueId	INT,
	ShowId INT,
	EventDate VARCHAR(10),
	EventTime VARCHAR(8),
	EventEnd VARCHAR(8),
	SchoolOnly INT,
	InStock INT
)

INSERT INTO @showTimes
(
	VenueId,
	ShowId,
	EventDate,
	EventTime,
	EventEnd,
	SchoolOnly,
	InStock
)
SELECT	DISTINCT 
	v.VenueId,
	s.ShowID,
	p.EventDate,
	p.EventTime,
	p.EventEnd,
	p.SchoolOnly,
	p.InStock
FROM    @prods p
INNER JOIN	@venues v
	ON	p.Venue = v.Venue
	AND	 p.MemberOnly = v.MemberOnly
	AND p.SoftCapacity = v.SoftCapacity
INNER JOIN @shows s
	ON s.EventTitleLong = p.EventTitleLong
	AND s.Location = p.Location
	AND s.EventNumber = p.EventNumber
	AND s.ItemLength = p.ItemLength

--SELECT 'showTimes', * FROM @showTimes


SET @OutputXML = 
(SELECT v.venue AS 'title',
	v.MemberOnly AS 'memberonly',
	v.SoftCapacity as 'capacity',
	(
	SELECT	s.EventTitleLong AS 'title',
			s.Location AS 'location',
			s.ItemLength as 'length',
			s.EventNumber AS 'eventnumber',
			(
			SELECT sdt.EventDate AS 'eventdate',
					(
					SELECT	times.SchoolOnly AS 'starttime/@schoolonly',
							times.EventTime AS 'starttime',
							times.SchoolOnly AS 'endtime/@schoolonly',
							times.EventEnd AS 'endtime',
							times.InStock AS 'instock'
					FROM	@showTimes times
					WHERE	times.ShowId = sdt.ShowId
						AND		times.VenueId = sdt.VenueId
						AND		times.EventDate = sdt.EventDate
					ORDER BY times.EventDate, times.EventTime, times.EventEnd
					FOR XML PATH ('time'), ROOT ('times'), TYPE
					)
			FROM	@showDates sdt
			WHERE	sdt.ShowId = s.ShowId
			AND		sdt.VenueId = s.VenueId
			ORDER BY sdt.EventDate
			FOR XML PATH ('date'), TYPE
			)			
	FROM	@shows s
	WHERE	s.VenueId = v.VenueId
	FOR XML PATH ('show'), ROOT ('shows'), TYPE	
	)
FROM	@venues v
ORDER BY v.Venue
FOR XML PATH ('venue'), ROOT ('products'), TYPE
)

SELECT @OutputXML AS outputXML


GO


